"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const webpack_1 = __importDefault(require("webpack"));
const webpack_merge_1 = require("webpack-merge");
const american_sports_oligarch_1 = require("american-sports-oligarch");
const webpack_common_1 = __importDefault(require("../webpack.common"));
const main_controller_1 = require("./main-controller");
// import { globalStoreDefinitions } from './store-definitions'
let config = webpack_merge_1.merge(webpack_common_1.default, {
    mode: 'development',
    devtool: 'source-map',
    entry: './src/index.ts'
});
const compiler = webpack_1.default(config);
const watcher = compiler.watch({}, async function (err) {
    let container = await american_sports_oligarch_1.getContainer();
    container.bind(main_controller_1.MainController).toSelf().inSingletonScope();
    let controller = container.get(main_controller_1.MainController);
    await controller.run();
});
//# sourceMappingURL=index.js.map
import { NbaDownloadService } from "american-sports-oligarch";
declare class MainController {
    private nbaDownloadService;
    constructor(nbaDownloadService: NbaDownloadService);
    run(): Promise<void>;
    downloadPlayers(): Promise<void>;
}
export { MainController };

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainController = void 0;
const prompt = require('prompt');
const space_mvc_1 = require("space-mvc");
const american_sports_oligarch_1 = require("american-sports-oligarch");
let MainController = class MainController {
    constructor(nbaDownloadService) {
        this.nbaDownloadService = nbaDownloadService;
    }
    async run() {
        let keepGoing = true;
        while (keepGoing) {
            console.log(`
      --------------------------------------
      Choose an option

      1. Add new players to contract.
      2. Exit
      --------------------------------------
    `);
            const selection = await prompt.get(['choice']);
            console.log(`Choice was: ${selection.choice}`);
            switch (selection.choice) {
                case "1":
                    await this.downloadPlayers();
                    break;
                case "2":
                    keepGoing = false;
                    break;
            }
        }
    }
    async downloadPlayers() {
        console.log("Starting download");
        await this.nbaDownloadService.downloadNewPlayers();
        console.log("Download complete");
    }
};
MainController = __decorate([
    space_mvc_1.controller(),
    __metadata("design:paramtypes", [typeof (_a = typeof american_sports_oligarch_1.NbaDownloadService !== "undefined" && american_sports_oligarch_1.NbaDownloadService) === "function" ? _a : Object])
], MainController);
exports.MainController = MainController;
//# sourceMappingURL=main-controller.js.map
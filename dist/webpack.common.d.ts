declare const _default: {
    entry: string;
    module: {
        rules: {
            test: RegExp;
            use: string[];
            exclude: RegExp;
        }[];
    };
    resolve: {
        extensions: string[];
    };
    output: {
        filename: string;
        library: string;
        path: string;
    };
    plugins: any[];
};
export default _default;

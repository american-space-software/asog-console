const { getContainer } = require("american-sports-oligarch");
const { MainController } = require('./main-controller');
async function main() {
    let container = await getContainer();
    container.bind(MainController).toSelf().inSingletonScope();
    let controller = container.get(MainController);
    await controller.run();
}
main();
//# sourceMappingURL=index.js.map
import { Container, Contract, ContractService, ethers, initTestContainer, MetastoreService, MfdbService } from "space-mvc";
import { nbaStatsDefinition, truffleJson, ImageService, PlayerService } from "american-sports-oligarch"

import { MainController } from '../src/controller/main-controller'
import { NbaDownloadService } from "../src/service/nba-download-service"
import { PlayerNameService } from "../src/service/player-name-service"

export async function getTestContainer() {

    let container = new Container()
  
 
    container.bind(ImageService).toSelf().inSingletonScope()
    container.bind(PlayerService).toSelf().inSingletonScope()
  
    container.bind(MainController).toSelf().inSingletonScope()
    container.bind(NbaDownloadService).toSelf().inSingletonScope()
    container.bind(PlayerNameService).toSelf().inSingletonScope()
    
    //Get Truffle contract
  
    let provider = new ethers.providers.JsonRpcProvider("http://localhost:7545")
    let contract:Contract = await ContractService.createContractFromTruffle(truffleJson)
  
  
    container = await initTestContainer(container, provider, provider.getSigner(0), undefined, [contract])


    //Connect NBA stats metastore
    let mfdbService:MfdbService = container.get(MfdbService)

  
    
    //Clear out old data
    await mfdbService.mfdb.dropDatabase("TEST_NBA_STATS")
    
    //Create
    nbaStatsDefinition.name = "TEST_NBA_STATS"
    await mfdbService.mfdb.createDatabase(nbaStatsDefinition)
    await mfdbService.mfdb.useDatabase("TEST_NBA_STATS")


    return container


}

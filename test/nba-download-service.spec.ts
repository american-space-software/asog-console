
const assert = require('assert')

import { NbaDownloadService } from "../src/service/nba-download-service"
import { getTestContainer } from "./test-inversify.config"
import { PlayerService } from "american-sports-oligarch"


describe('NbaDownloadService', async () => {    

    let downloadService: NbaDownloadService
    let playerService: PlayerService

    before('Main setup', async () => {
        let container = await getTestContainer()
        playerService = container.get(PlayerService)
        downloadService = container.get(NbaDownloadService)
    })

    it("should download all current players", async () => {

        await downloadService.buildPlayerTable()
        let players = await downloadService.insertPlayers()
        // assert.strictEqual(players.length > 0, true)

    })

    it("should download and insert new players into contract and IPFS", async () => {

        //Arrange
        // let playerCount = await playerService.countPlayers()
        // console.log(playerCount)

        //Act
        // await downloadService.downloadNewPlayers()

        //Assert
        // let afterCount = await playerService.countPlayers()
        // console.log(afterCount)


    })



})



import { CreatePlayerCommand, PlayerService, PlayerInfoDao, PlayerSeasonStatsDao, PlayerInfo, PlayerSeasonAvgStats, Team, TeamDao } from "american-sports-oligarch"
import { service } from "space-mvc"
import { NbaDownloadService, NbaFantasyStats, NbaPlayerGeneralStats, NbaPlayerInfo, NbaTeam, PlayerSalary } from "./nba-download-service"
const fs = require('fs')

let SCORE_FG3 = 3
let SCORE_FG2 = 2
let SCORE_FT = 1
let SCORE_REB = 1.2 
let SCORE_AST = 1.5 
let SCORE_BLK = 3
let SCORE_STL = 3
let SCORE_TOV = -1



@service()
class BuildDatabaseService {

    constructor(
        private playerService:PlayerService,
        private playerInfoDao:PlayerInfoDao,
        private playerSeasonStatsDao:PlayerSeasonStatsDao,
        private teamDao:TeamDao,
        private nbaDownloadService:NbaDownloadService
    ) { }

    async buildAllTables() {
        
        await this.buildTeamsTable()
        await this.buildPlayerTable()
        await this.buildPlayerSeasonStatsTable()

    }

    async buildPlayerTable() {

        //Get the list for the current season.
        let playerInfo:NbaPlayerInfo[] = await this.nbaDownloadService.downloadCurrentSeasonPlayers()


        let nbaFantasyStats:NbaFantasyStats[] = await this.nbaDownloadService.downloadFantasyStats()

        let mapped:PlayerInfo[] = playerInfo.map( player => {

            //Get general stats
            let fantasyStats:NbaFantasyStats = nbaFantasyStats.filter( stats => stats.PLAYER_ID == player.PERSON_ID)[0]

            let positions:string[] = []
            
            if (fantasyStats && fantasyStats.PLAYER_POSITION) {
                positions = fantasyStats.PLAYER_POSITION.split("-")
            }


            return {
                displayFirstLast: player.DISPLAY_FIRST_LAST,
                displayLastCommaFirst: player.DISPLAY_LAST_COMMA_FIRST,
                playerKey: player.PERSON_ID.toString(),
                teamId: player.TEAM_ID.toString(),
                positions: positions
            }
        })

        //Save
        await this.playerInfoDao.insert(mapped)


    }

    async buildPlayerSeasonStatsTable() {
        
        //Download fantasy stats
        let stats:NbaPlayerGeneralStats[] = await this.nbaDownloadService.downloadLeaguePlayerGeneralStats()

        let mapped:PlayerSeasonAvgStats[] = stats.map( stat => {
            
            let FG3M = stat.FG3M ? stat.FG3M : 0
            let FGM = stat.FGM ? stat.FGM : 0
            let FTM = stat.FTM ? stat.FTM : 0
            let REB = stat.REB ? stat.REB : 0
            let AST = stat.AST ? stat.AST : 0 
            let BLK = stat.BLK ? stat.BLK : 0
            let STL = stat.STL ? stat.STL : 0 
            let TOV = stat.TOV ? stat.TOV : 0 

            let GP = 0
            let avg = 0
            let total = 0

            if (stat.GP > 0) {
                GP = stat.GP 
                total = this.calculateScore(FG3M, FGM, FTM, REB, AST, BLK, STL, TOV)
                avg = total / stat.GP
            }

            return {
                gamesPlayed: GP,
                assists: stat.AST,
                blocks: stat.BLK,

                fieldGoalAttempts: stat.FGA,
                fieldGoalsMade: stat.FGM,
                fieldGoalPercent: stat.FG_PCT,

                freeThrowPercent: stat.FT_PCT,
                freeThrowAttempts: stat.FTA,
                freeThrowMade: stat.FTM,

                minutes: stat.MIN,
                playerKey: stat.PLAYER_ID.toString(),
                points: stat.PTS,
                rebounds: stat.REB,
                steals: stat.STL,

                threePointsAttempts: stat.FG3A,
                threePointsMade: stat.FG3M,
                threePointsPercent: stat.FG3_PCT,

                turnovers: stat.TOV,

                total: total,
                avg: avg
            }
        })

        //Save
        await this.playerSeasonStatsDao.insert(mapped)

    }

    async buildTeamsTable() {
        
        //Download fantasy stats
        let stats:NbaTeam[] = await this.nbaDownloadService.downloadTeams()

        let mapped:Team[] = stats.map( team => {
            return {
                teamId: team.TeamID.toString(),
                teamAbbreviation: team.Abbrev
            }
        })

        //Save
        await this.teamDao.insert(mapped)

    }



    private calculateScore(FG3M:number, FGM:number, FTM:number, REB:number, AST:number, BLK:number, STL:number, TOV:number) {
        return (FG3M * SCORE_FG3) + 
        ( (FGM - FG3M) * SCORE_FG2 ) + 
        (FTM * SCORE_FT) + 
        (REB * SCORE_REB) + 
        (AST * SCORE_AST) + 
        (BLK * SCORE_BLK) + 
        (STL * SCORE_STL) + 
        (TOV * SCORE_TOV)
    }

    async insertPlayers() {

        let commands:CreatePlayerCommand[] = await this.getCurrentSeasonCreatePlayerCommands()

        for (let command of commands) {
           
            let player:PlayerInfo = await this.playerInfoDao.get(command.playerKey.toString())

            //Check if player is in contract
            let existingPlayer = await this.playerService.getPlayerByPlayerKey(command.playerKey)

            if (existingPlayer) {
                console.log(`Skipping ${player.displayFirstLast}. Already exists.`)
            } else {
                let addedPlayer = await this.playerService.create(command)
                console.log(`Added ${player.displayFirstLast}`)
            }

            //If not then create

        }

    }






    private async getCurrentSeasonCreatePlayerCommands() : Promise<CreatePlayerCommand[]> {

        let commands:CreatePlayerCommand[] = []

        //Get the list for the current season.
        let players:PlayerInfo[] = await this.playerInfoDao.getCurrentSeasonPlayers()

        //Get salaries from BBREF
        let salaries:PlayerSalary[] = await this.nbaDownloadService.downloadSalaries()

        for (let player of players) {

            //Find salary
            let salary:PlayerSalary = salaries.filter( p => p.name == player.displayLastCommaFirst)[0]

            if (!salary) {
                console.log(`${player.displayLastCommaFirst} salary not found`)
                continue
            }

            let create:CreatePlayerCommand = {
                playerKey: parseInt(player.playerKey),
                sport: 1,
                salary: salary.salary
            }

            commands.push(create)

        }

        return commands

    }




}




export {
    BuildDatabaseService, PlayerSalary
}





// let playerRows = document.querySelectorAll('#player-contracts tbody tr:not(.over_header):not(.thead)')

// return Array.from(playerRows).map( row => {
    
//     let children = row.children 
//     let name = children[1].children[1].innerHTML
//     let key = children[1].getAttribute("data-append-csv")
//     let team = children[2].children[0].innerHTML 
//     let salary = children[3].innerHTML.replace("$", "").replaceAll(",", "")

//     return {
//         name: name,
//         key: key,
//         team: team,
//         salary: salary
//     }
// })
import { service } from "space-mvc"
import { PlayerNameService } from "./player-name-service"

const nba = require('nba-api-client')
const puppeteer = require('puppeteer')


import nbaTeams from "../dao/teams.json"


let CURRENT_SEASON = "2020-21"

@service()
class NbaDownloadService {

    constructor(
        private playerNameService:PlayerNameService
    ) { }

    public async downloadCurrentSeasonPlayers() : Promise<NbaPlayerInfo[]> {

        let players:NbaPlayerInfo[] = []

        let result = await nba.allPlayersList({
            IsOnlyCurrentSeason: "1",
            Season: CURRENT_SEASON
        })
        
        for (const key in result.CommonAllPlayers) {
            players.push(result.CommonAllPlayers[key])
        }

        console.log(`${players.length} players found.`)

        return players

    }


    public async downloadFantasyStats() : Promise<NbaFantasyStats[]> {

        let currentFantasyStats:NbaFantasyStats[] = []

        let results = await nba.fantasyStats({
            Season: CURRENT_SEASON
        })

        let fantasyWidgetResult = results['FantasyWidgetResult']


        for (let key in fantasyWidgetResult) {
            currentFantasyStats.push(fantasyWidgetResult[key])
        }

        return currentFantasyStats

    }


    public async downloadLeaguePlayerGeneralStats() : Promise<NbaPlayerGeneralStats[]> {

        let stats:NbaPlayerGeneralStats[] = []

        let results = await nba.leaguePlayerGeneralStats({
            Season: CURRENT_SEASON
        })

        let leagueDashPlayerStats = results['LeagueDashPlayerStats']

        for (let key in leagueDashPlayerStats) {
            stats.push(leagueDashPlayerStats[key])
        }

        return stats

    }

    public async downloadTeams() : Promise<NbaTeam[]> {

        let teams:NbaTeam[] = []

        for (let key in nbaTeams) {
            teams.push(nbaTeams[key])
        }

        return teams

    }

    public async downloadSalaries() : Promise<PlayerSalary[]> {

        const browser = await puppeteer.launch()
        const page = await browser.newPage()
        await page.goto('https://hoopshype.com/salaries/players/', {waitUntil: 'networkidle2'})

        const salaries:PlayerSalary[] = await page.evaluate( () => {
            
            //@ts-ignore
            let players = jQuery(".hh-salaries-ranking-table tbody tr");

            let records = []
            for (let i = 0; i < 568; i++) {
            
                //@ts-ignore
                let player = jQuery(players[i])
            
                if (player.length > 0) {
                    let record = {
                        name: player.children('.name').children('a').text().trim(),
                        salary: player.children('.hh-salaries-sorted').html().trim().replace("$", "").replaceAll(",", "")
                    }
                
                    records.push(record)
                }
            
            }
            
            return records


        })

        for (let salary of salaries) {
            this.playerNameService.fixName(salary)
        }

        return salaries

    }

}




interface PlayerSalary {
    name: string
    salary: number 
}



interface NbaPlayerInfo {
    PERSON_ID?: number
    DISPLAY_LAST_COMMA_FIRST?:string 
    DISPLAY_FIRST_LAST?: string
    ROSTERSTATUS?: number
    FROM_YEAR?: string
    TO_YEAR?: string
    PLAYERCODE?: string
    TEAM_ID?: number
    TEAM_CITY?: string
    TEAM_NAME?: string
    TEAM_ABBREVIATION?: string
    TEAM_CODE?: string
    GAMES_PLAYED_FLAG?: string
    OTHERLEAGUE_EXPERIENCE_CH?: string
}

interface NbaFantasyStats {
    PLAYER_ID: number
    PLAYER_NAME: string
    PLAYER_POSITION: string
    TEAM_ID: number
    TEAM_ABBREVIATION: string
    GP: number
    MIN: number
    FAN_DUEL_PTS: number
    NBA_FANTASY_PTS: number
    PTS: number
    REB: number
    AST: number
    BLK: number
    STL: number
    TOV: number
    FG3M: number
    FGA: number
    FG_PCT: number
    FTA: number
    FT_PCT: number
}

interface NbaPlayerGeneralStats {
    PLAYER_ID: number
    PLAYER_NAME: string
    TEAM_ID: number
    TEAM_ABBREVIATION: string
    AGE: number
    GP: number
    W: number
    L:  number
    W_PCT:  number
    MIN:  number
    FGM:  number
    FGA:  number
    FG_PCT:  number
    FG3M:  number
    FG3A:  number
    FG3_PCT:  number
    FTM:  number
    FTA:  number
    FT_PCT:  number
    OREB:  number
    DREB:  number
    REB:  number
    AST:  number
    TOV: number
    STL:  number
    BLK:  number
    BLKA:  number
    PF:  number
    PFD:  number
    PTS:  number
}

interface NbaTeam {
    TeamID: number,
    Abbrev: string,
    TeamName: string,
    City: string 
}

export {
    NbaDownloadService, NbaPlayerInfo, NbaFantasyStats, NbaPlayerGeneralStats, PlayerSalary, NbaTeam
}
import { service } from "space-mvc"
import {  PlayerSalary } from "./build-database-service"

@service()
class PlayerNameService {

    constructor() {}

    fixName(salary:PlayerSalary) {
        
        if (salary.name.indexOf(" Jr") > 0) {
            salary.name = salary.name.replace(" Jr", " Jr.")
        }

        let split = salary.name.split(" ")

        if (salary.name.indexOf("Jr.") > 0 || salary.name.indexOf("III") > 0) {
            salary.name = `${split[1]} ${split[2]}, ${split[0]}`
        } else {
            salary.name = `${split[1]}, ${split[0]}`
        }

        let namePairs = {
            "Ennis, James": "Ennis III, James",
            "Bagley, Marvin": "Bagley III, Marvin",
            "Augustin, DJ": "Augustin, D.J.",
            "Bembry, DeAndre": "Bembry, DeAndre'",
            "Bowen, Brian": "Bowen II, Brian",
            "Brown, Troy": "Brown Jr., Troy",
            "Carey, Vernon": "Carey Jr., Vernon",
            "Carter, Wendell": "Carter Jr., Wendell",
            "Giles, Harry": "Giles III, Harry",
            "Graham, Devonte": "Graham, Devonte'",
            "Hampton, RJ": "Hampton, R.J.",
            "Harkless, Moe": "Harkless, Maurice",
            "Hernangomez, Juan": "Hernangomez, Juancho",
            "House, Danuel": "House Jr., Danuel",
            "Iwundu, Wesley": "Iwundu, Wes",
            "Jackson, Jaren": "Jackson Jr., Jaren",
            "Jones, Derrick": "Jones Jr., Derrick",
            "Knox, Kevin": "Knox II, Kevin",
            "Lewis, Kira": "Lewis Jr., Kira",
            "Luwawu, Timothe": "Luwawu-Cabarrot, Timothe",
            "Martin, KJ": "Martin Jr., Kenyon",
            "McConnell, TJ": "McConnell, T.J.",
            "Mills, Patrick": "Mills, Patty",
            "Morris, Marcus": "Morris Sr., Marcus",
            "Mykhailiuk, Sviatoslav": "Mykhailiuk, Svi",
            "Oubre, Kelly": "Oubre Jr., Kelly",
            "Porter, Kevin": "Porter Jr., Kevin",
            "Porter, Michael": "Porter Jr., Michael",
            "Porter, Otto": "Porter Jr., Otto",
            "Schroeder, Dennis": "Schroder, Dennis",
            "Smith, Ishmael": "Smith, Ish",
            "Smith, Dennis": "Smith Jr., Dennis",
            "Tucker, PJ": "Tucker, P.J.",
            "Walker, Lonnie": "Walker IV, Lonnie",
            "Warren, TJ": "Warren, T.J.",
            "Washington, PJ": "Washington, P.J.",
            "Williams, Louis": "Williams, Lou",
            "Williams, Robert": "Williams III, Robert",
            "Wilson, DJ": "Wilson, D.J.",
            "Woodard, Robert": "Woodard II, Robert"
        }

        for (let original in namePairs) {
            this.fixSpecific(salary, original, namePairs[original])
        }


        
    }

    fixSpecific(salary, before, after) {
        if (salary.name == before) {
            salary.name = after       
        }
    }

}

export {
    PlayerNameService
}
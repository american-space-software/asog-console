import { IpfsService } from "space-mvc"

const Libp2p = require('libp2p')
const WebRtcStar = require('libp2p-webrtc-star')
const { NOISE } = require('libp2p-noise')
const MPLEX = require('libp2p-mplex')
const wrtc = require('wrtc')

const IPFS = require('ipfs')


let options = {

	modules: {
		transport: [WebRtcStar],
		connEncryption: [NOISE],
		streamMuxer: [MPLEX]
	},
	addresses: {
		listen: [
			'/dns4/wrtc-star1.par.dwebops.pub/tcp/443/wss/p2p-webrtc-star'
		]
	},
	config: {
		transport: {
			[WebRtcStar.prototype[Symbol.toStringTag]]: {
				wrtc
			}
		},
		peerDiscovery: {
			autoDial: true,
			webRTCStar: {
				enabled: true
			}
		}
	}
}



let libp2pBundle = (opts) => {
    
    // Set convenience variables to clearly showcase some of the useful things that are available
    const peerId = opts.peerId;
    const bootstrapList = opts.config.Bootstrap;

	options['peerId'] = peerId

    // Build and return our libp2p node
    return new Libp2p(options)
}


async function run() {

	// const node = await Libp2p.create(options)

	// // start libp2p
	// await node.start()

	// const advertiseAddrs = node.multiaddrs
	// console.log('libp2p is advertising the following addresses: ', advertiseAddrs)

	// // stop libp2p
	// await node.stop()


	let ipfs = await IPFS.create({
		libp2p: libp2pBundle,
		repo: '../dist/console-repo',
		EXPERIMENTAL: {
			pubsub: true
		},
		config: {
			Addresses: {
				Swarm: [
					'/dns4/rocky-sands-15324.herokuapp.com/tcp/443/wss/p2p-webrtc-star/',
					// '/dns4/wrtc-star1.par.dwebops.pub/tcp/443/wss/p2p-webrtc-star/',
					// '/dns4/wrtc-star2.sjc.dwebops.pub/tcp/443/wss/p2p-webrtc-star/',
					// '/dns4/webrtc-star.discovery.libp2p.io/tcp/443/wss/p2p-webrtc-star/',
					// '/dns4/libp2p-rdv.vps.revolunet.com/tcp/443/wss/p2p-webrtc-star/'
				]
			}
		}
	})







}




run()





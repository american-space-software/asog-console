
const Libp2p = require('libp2p')
const WebRtcStar = require('libp2p-webrtc-star')
const { NOISE } = require('libp2p-noise')
const MPLEX = require('libp2p-mplex')
const wrtc = require('wrtc')
const KadDHT = require('libp2p-kad-dht')
const Gossipsub = require('libp2p-gossipsub')


let options = {
    // Lets limit the connection managers peers and have it check peer health less frequently
    connectionManager: {
        minPeers: 25,
        maxPeers: 100,
        pollInterval: 5000
    },
    modules: {
        transport: [WebRtcStar],
        connEncryption: [NOISE],
        streamMuxer: [MPLEX],
        dht: KadDHT,
        pubsub: Gossipsub
    },
    addresses: {
        listen: [
            '/dns4/wrtc-star1.par.dwebops.pub/tcp/443/wss/p2p-webrtc-star'
        ]
    },
    config: {
        pubsub: {
            emitSelf: true
        },
        transport: {
            [WebRtcStar.prototype[Symbol.toStringTag]]: {
                wrtc
            }
        },
        peerDiscovery: {
            autoDial: true,
            webRTCStar: {
                enabled: true
            }
        },
        relay: {
            enabled: true,
            hop: {
                enabled: true,
                active: true
            }
        },
        dht: {
            enabled: true,
            randomWalk: {
                enabled: true,
            }
        }
    }
}



/**
 * This is the factory we will use to create our fully customized libp2p bundle.
 *
 * @param {libp2pBundle~options} opts The options to use when generating the libp2p node
 * @returns {Libp2p} Our new libp2p node
 */
export default (opts) => {

    // Set convenience variables to clearly showcase some of the useful things that are available
    const peerId = opts.peerId;
    const bootstrapList = opts.config.Bootstrap;

    options['peerId'] = peerId


    // Build and return our libp2p node
    return new Libp2p(options)

}
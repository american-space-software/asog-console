import { Container, Contract, ContractService, ethers, initTestContainer, MetastoreService, MfdbService } from "space-mvc"

import { truffleJson, ImageService, PlayerService, nbaStatsDefinition, PlayerInfoDao, PlayerSeasonStatsDao, TeamDao } from "american-sports-oligarch"
import { MainController } from './controller/main-controller'
import { BuildDatabaseService } from "./service/build-database-service"
import { PlayerNameService } from "./service/player-name-service"

import libp2pBundle from "./libp2p"
import { NbaDownloadService } from "./service/nba-download-service"


async function main() {
  
  let container = new Container()

  container.bind(ImageService).toSelf().inSingletonScope()
  container.bind(PlayerService).toSelf().inSingletonScope()
  container.bind(PlayerNameService).toSelf().inSingletonScope()
  container.bind(MainController).toSelf().inSingletonScope()
  container.bind(BuildDatabaseService).toSelf().inSingletonScope()

  container.bind(PlayerInfoDao).toSelf().inSingletonScope()
  container.bind(PlayerSeasonStatsDao).toSelf().inSingletonScope()
  container.bind(TeamDao).toSelf().inSingletonScope()
  container.bind(NbaDownloadService).toSelf().inSingletonScope()


  container.bind("ipfsOptions").toConstantValue(
    {
      libp2p: libp2pBundle,
      repo: '../dist/console-repo',
      EXPERIMENTAL: {
        pubsub: true
      },
      config: {
        Addresses: {
          Swarm: [
            '/dns4/rocky-sands-15324.herokuapp.com/tcp/443/wss/p2p-webrtc-star/',
            // '/dns4/wrtc-star1.par.dwebops.pub/tcp/443/wss/p2p-webrtc-star/',
            // '/dns4/wrtc-star2.sjc.dwebops.pub/tcp/443/wss/p2p-webrtc-star/',
            // '/dns4/webrtc-star.discovery.libp2p.io/tcp/443/wss/p2p-webrtc-star/',
            // '/dns4/libp2p-rdv.vps.revolunet.com/tcp/443/wss/p2p-webrtc-star/'
          ]
        }
      }
    }
  )


  //Get Truffle contract

  let provider = new ethers.providers.JsonRpcProvider("http://localhost:7545")
  let contract: Contract = await ContractService.createContractFromTruffle(truffleJson)

  container = await initTestContainer(container, provider, provider.getSigner(0), undefined, [contract])
      
  //Connect NBA stats metastore
  let mfdbService:MfdbService = container.get(MfdbService)

  
  //Clear out old data
  // await mfdbService.mfdb.dropDatabase("NBA_STATS")
  
  // //Create
  // if (!await mfdbService.mfdb.databaseExists("NBA_STATS")) {
  //   await mfdbService.mfdb.createDatabase(nbaStatsDefinition)
  // }

  await mfdbService.mfdb.useDatabase("NBA_STATS")

  let controller = container.get(MainController)

  await controller.run()


}






main()






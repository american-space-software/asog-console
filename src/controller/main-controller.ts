import { PlayerInfo, PlayerInfoDao } from 'american-sports-oligarch';
import readline from 'readline-promise'

import { controller, IpfsService, MfdbService } from 'space-mvc'
import { BuildDatabaseService } from '../service/build-database-service'

@controller()
class MainController {

  constructor(
    private buildDatabaseService: BuildDatabaseService,
    private mfdbService:MfdbService,
    private nbaPlayerInfoDao:PlayerInfoDao
  ) { }

  async run() {

    const rlp = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
      terminal: true
    })

    let keepGoing = true;

    while (keepGoing) {

      let menu = `
      --------------------------------------
      Choose an option

      1. Build all tables.
      2. Build teams table.
      3. Build player table.
      4. Build player season stats table.
      5. Insert players into contract.
      6. Show player list.
      7. Show current CID.
      8. Exit
      --------------------------------------
      `

      let choice = await rlp.questionAsync(menu)

      console.log(`Choice was: ${choice}`)

      switch (choice) {

        case "1":
          await this.buildAllTables()
          break;

        case "2":
          await this.buildTeamsTable()
          break;

        case "3":
          await this.buildPlayerTable()
          break;

        case "4":
          await this.buildPlayerSeasonStatsTable()
          break

        case "5":
          await this.insertNewPlayers()
          break

        case "6":
          await this.showPlayerList()
          break

        case "7":
          await this.showCurrentCID()
          break
        
        case "8":
          keepGoing = false
          break
      }

    }

    console.log("Exiting...")


  }

  
  async showCurrentCID() {
    console.log(`Current CID is ${await this.mfdbService.mfdb.getDatabaseCID("NBA_STATS")}`)
  }

  async buildAllTables() {
    console.log("Building all tables")
    await this.buildDatabaseService.buildAllTables()
    console.log("Building all tables complete")
  }


  async buildTeamsTable() {
    console.log("Building teams table")
    await this.buildDatabaseService.buildTeamsTable()
    console.log("Building teams table complete")
  }

  async buildPlayerTable() {
    console.log("Building player table")
    await this.buildDatabaseService.buildPlayerTable()
    console.log("Building player table complete")
  }

  async buildPlayerSeasonStatsTable() {
    console.log("Starting download")
    await this.buildDatabaseService.buildPlayerSeasonStatsTable()
    console.log("Download complete")
  }


  async insertNewPlayers() {
    console.log("Inserting new players")
    await this.buildDatabaseService.insertPlayers()
    console.log("Inserting new players complete")
  }

  async showPlayerList() {
    
    console.log("Showing all players")
    
    let players:PlayerInfo[] = await this.nbaPlayerInfoDao.getCurrentSeasonPlayers()

    for (let player of players) {
      console.log(`${player.displayFirstLast} #${player.playerKey}`)
    }

    console.log("Showing all players")
  }

}


export {
    MainController
}